#------------------------------------------------------------------------------
#                  GEOS-Chem Global Chemical Transport Model                  #
#------------------------------------------------------------------------------
#BOP
#
# !MODULE: Makefile
#
# !DESCRIPTION: This is a simple makefile for the GCHP regrid utility.
#\\
#\\
# !REMARKS:
# To build the programs, call "make" with the following syntax:
#                                                                             .
#   make -jN TARGET REQUIRED-FLAGS [ OPTIONAL-FLAGS ]
#                                                                             .
# To display a complete list of options, type "make help".
#                                                                             .
# Makefile uses the following variables:
#                                                                             .
# Variable   Description
# --------   -----------
# SHELL      Specifies the shell for "make" to use (usually SHELL=/bin/sh)
# NCDF       Specifies the directory where netCDF utilities are stored
#
# !REVISION HISTORY: 
#  09 Jan 2017 - S. D. Eastham - Initial version, based on Bob Yantosca's GC work
#

ifndef COMPILER
  COMPILER           :=$(FC)
endif

# Basic...
COMPILE_CMD        :=$(FC)

# Get NetCDF info, based on GC requirements
ifdef NETCDF_FORTRAN_INCLUDE
  NC_INC_CMD         := -I$(NETCDF_INCLUDE) -I$(NETCDF_FORTRAN_INCLUDE)
else
  NC_INC_CMD         := -I$(NETCDF_INCLUDE)
endif

# Get the version number (e.g. "4130"=netCDF 4.1.3; "4200"=netCDF 4.2, etc.)
NC_VERSION           :=$(shell $(NETCDF_HOME)/bin/nc-config --version)
NC_VERSION           :=$(shell echo "$(NC_VERSION)" | sed 's|netCDF ||g')
NC_VERSION           :=$(shell echo "$(NC_VERSION)" | sed 's|\.||g')
NC_VERSION_LEN       :=$(shell perl -e "print length $(NC_VERSION)")
ifeq ($(NC_VERSION_LEN),3)
 NC_VERSION          :=$(NC_VERSION)0
endif
ifeq ($(NC_VERSION_LEN),2) 
 NC_VERSION          :=$(NC_VERSION)00
endif

# Test if we have at least netCDF 4.2.0.0
AT_LEAST_NC_4200     :=$(shell perl -e "print ($(NC_VERSION) ge 4200)")

ifeq ($(AT_LEAST_NC_4200),1) 

  #-------------------------------------------------------------------------
  # netCDF 4.2 and higher:
  # Use "nf-config --flibs" and "nc-config --libs"
  # Test if a separate netcdf-fortran path is specified
  #-------------------------------------------------------------------------
  ifdef NETCDF_FORTRAN_HOME
    NC_LINK_CMD      := $(shell $(NETCDF_FORTRAN_HOME)/bin/nf-config --flibs)
  else
    NC_LINK_CMD      := $(shell $(NETCDF_HOME)/bin/nf-config --flibs)
  endif
  NC_LINK_CMD        += $(shell $(NETCDF_HOME)/bin/nc-config --libs)

else

  #-----------------------------------------------------------------------
  # Prior to netCDF 4.2:
  # Use "nc-config --flibs" and nc-config --libs
  #-----------------------------------------------------------------------
  NC_LINK_CMD        := $(shell $(NETCDF_HOME)/bin/nc-config --flibs)

endif

# Base linker command: specify the library directory
LINK                 :=-L$(NC_LINK_CMD)

ifeq ($(COMPILER),ifort) 
  # Include options
  INCLUDE            :=$(NC_INC_CMD)
  FFLAGS             :=-cpp -w -auto -noalign -convert big_endian

  # Debug
  ifeq ($(DEBUG),yes)
     FFLAGS             += -g -O0 -check arg_temp_created -debug all
     FFLAGS             += -fpe0 -ftrapuv
     FFLAGS             += -check bounds
     FFLAGS             += -check all -warn all
  endif
  FFLAGS             += -traceback
 
  # Standard
  #OPT                := -O2
  #FFLAGS             += $(OPT) -vec-report0

  # Prevent any optimizations that would change numerical results
  FFLAGS             += -fp-model source

  # Turn on OpenMP parallelization
  FFLAGS             += -openmp

  CC                 :=
  F90                :=$(COMPILE_CMD) $(FFLAGS) $(INCLUDE)
  LD                 :=$(COMPILE_CMD) $(FFLAGS)
  FREEFORM           := -free
else ifeq ($(COMPILER),gfortran)
  # Get the GNU Fortran version
  GNU_VERSION        :=$(shell $(FC) -dumpversion)
  GNU_VERSION        :=$(subst .,,$(GNU_VERSION))
  NEWER_THAN_447     :=$(shell perl -e "print ($(GNU_VERSION) gt 447)")

  # Base set of compiler flags
  INCLUDE            :=$(NC_INC_CMD)
  FFLAGS             :=-cpp -w -std=legacy -fautomatic -fno-align-commons
  FFLAGS             += -fconvert=big-endian
  FFLAGS             += -fno-range-check

  # Default optimization level for all routines (-O3)
  FFLAGS             += -O3 -funroll-loops

  # Pick compiler options for debug run or regular run 
  #FFLAGS           += -g -gdwarf-2 -gstrict-dwarf -O0
  #FFLAGS           += -Wall -Wextra -Wconversion
  #FFLAGS           += -Warray-temporaries -fcheck-array-temporaries
  #FFLAGS           += -ffpe-trap=invalid,zero,overflow
  #ifeq ($(NEWER_THAN_447),1)
  #  FFLAGS           += -finit-real=snan
  #endif
  #FFLAGS           += -fbounds-check

  # Allow OpenMP
  FFLAGS           += -fopenmp

  FFLAGS           += -mcmodel=medium

  FFLAGS           += -g -fbacktrace

  # Set the standard compiler variables
  CC                 :=
  F90                :=$(COMPILE_CMD) $(FFLAGS) $(INCLUDE)
  LD                 :=$(COMPILE_CMD) $(FFLAGS)
  FREEFORM           := -ffree-form -ffree-line-length-none
endif

%.o : %.F90
	$(F90) -c $(FREEFORM) $<

# List of source files: everything ending in .F or .F90
SOURCES :=$(wildcard *.F) $(wildcard *.F90)

# List of object files (replace .F and .F90 with .o)
TMP     :=$(SOURCES:.F=.o)
OBJECTS :=$(TMP:.F90=.o)

.PHONY: clean

regrid: gchp_regrid.o
	$(LD) $(LINK) $(OBJECTS) -o regrid

clean:  
	@rm -f *.o regrid

gchp_regrid.o: gchp_regrid.F90 regrid_functions_mod.o

regrid_functions_mod.o: regrid_functions_mod.F90 
